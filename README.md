# Download Adobe Brackets
Adobe Brackets is being depricated. This is an original archive of Adobe Brackets. That means that nothing is different from the original one.
The original code is also free and open-source at <a href="https://github.com/fakerybakery/brackets/" target="_blank">github.com/fakerybakery/brackets</a>. This is also a original remix.
The application download is too big for GitHub, so I am trying BitBucket.

Thanks for using!